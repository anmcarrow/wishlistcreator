## Просто вишлист.  

Т.е. - лист тех «маленьких хренек» которые мне надо, но не необходимо. Тех самых штук, что обычно отступают под напором более актуальных финансовых затрат *(как-то: коммунальные платежи, обихаживание любимой женщины, организация мероприятий, etc.)*.

Их можно дарить по поводу и без. Они, в любом случае, будут хорошим подарком.

# Настольно-ролевое

## Всеразличные тематические наклейки
![](https://s-media-cache-ak0.pinimg.com/736x/0d/0c/50/0d0c500288b6500cf0d5f89be3898cff.jpg)
Не обязательно на русском. Не брендированные. Забавные.

## Plano storage box model 737  
![Plano storage box model 737](http://learningdm.files.wordpress.com/2011/05/imgp3431.jpg?w=640&h=480)
[Купить, например, тут](http://www.voblera.net/catalog/yaschiki/plano-737-001)
 
## Или Flambeau 8010 FRONT LOADER  
![Flambeau 8010 FRONT LOADER](http://www.vseinstrumenti.ru/images/goods/sadovaya_tehnika/tovary_dlya_otdyha/532209/490x280/899141.jpg)

## Stack the Bones game от Kickerland
![Stack the bones!](https://lh3.googleusercontent.com/-lpBgjVBVrU4/VAN5rDSFd3I/AAAAAAAAPSo/ZyF6SkHEu38/w448-h597-no/14%2B-%2B1)
[Купить, например, тут](http://www.ebay.com/sch/i.html?_from=R40&_sacat=0&_nkw=Stack+the+Bones+Game&_sop=15)

## D&D-совместимые миниатюрки Всякие (но лучше - крашеные), можно даже Б/У  
![Миниатюрки, тысячи их!](http://g-ecx.images-amazon.com/images/G/01/ciu/10/d0/f501e03ae7a081a1fd971210.L.jpg)

# Гаджеты и прочая техника  

## Чехол для PSP, "жёсткий"
![](https://mmedia2.ozone.ru/multimedia/spare_covers/1005478726.jpg)

Например, [ARTPLAYS EVA Pouch Carbon](https://www.ozon.ru/context/detail/id/18077029/) (в качестве пенала для рисовательных принадлежностей).

## Педали для ПК
![](http://img.dxcdn.com/productimages/sku_56508_1.jpg)
> "For those who use computer in alternative ways."©

Брать [здесь](http://www.kinesis-ergo.com/shop/savant-elite2-waterproof-triple-pedal/) или, на худой конец — [здесь](http://www.dx.com/p/usb-triple-action-foot-switch-keyboard-control-foot-pedal-56508#.VmFnZFjhDRY).

## "Однорукая" клавиатура
![](http://www.hwp.ru/Joysticks/Cybersnipa.gamepad/0.jpg)
[Belkin Nostromo n52](https://market.yandex.ru/product/3606350?hid=723088&show-uid=019760414388542251), 
[ErgoMedia 500](https://market.yandex.ru/product/1015955?hid=723088&nid=55335&text=Genius%20ErgoMedia%20500&srnum=1),
[Cyber Snipa Game Pad](http://www.hwp.ru/articles/Igrovaya_klaviatura_Cyber_Snipa_Game_Pad/)
или простецкая [X7-G100](https://market.yandex.ru/product/986018?hid=723088&track=alts).  
*Razer и Logitech не предлагать!*  
Зачем? В дополнение к wacom-дигитайзеру, конечно.

## Убермышь "Swiftpoint Mouse"
![Swiftpoint Mouse as IS](http://zapp0.staticworld.net/reviews/graphics/products/uploaded/swiftpoint_limited_swiftpoint_mouse_799212_g4.jpg)
Реально крутая и чертовски удобная для меня мышь, всего за 50 Евро.  
[Купить тут](http://www.europe.futuremouse.com/shop/Swiftpoint+Products/Swiftpoint+Mouse-2.html)

## Ну или хотя бы Kreolz WME 153 Black
![](http://mdata.yandex.net/i?path=b0217220542_img_id3970809943591142235.jpg)
[Купить тут](https://market.yandex.ru/product/7869695?hid=723088)

## Трекбол Logitech TrackMan Trackball 
![Logitech TrackMan Trackball](http://i.testfreaks.com/images/products/600x400/234/logitech-cordless-trackman-optical.9370346.jpg)

## SteelSeries 7h for iPad, iPod и iPhone  
![](https://mdata.yandex.net/i?path=b0402191235_img_id4797168154703977373.jpg)
Для моей Apple-техники (не USB-версию!).  
[Есть на e-bay.](http://www.ebay.com/sch/i.html?_odkw=SteelSeries+7H+iphone+ipad&_osacat=0&_from=R40&_trksid=p2045573.m570.l1313.TR0.TRC0.H0.XSteelSeries+7H+ipad.TRS0&_nkw=SteelSeries+7H+ipad&_sacat=0)

## Аудио-гарнитуру, беспроводную, USB, с мониторными наушниками  
![Creative Sound Blaster Tactic 3D Wrath](http://mdata.yandex.net/i?path=b1225125238_img_id5052509219088001043.jpg)
Например — Creative Sound Blaster Tactic 3D Wrath  
[Купить тут](http://market.yandex.ru/model.xml?modelid=7808503&hid=6368403&text=Creative%20Sound%20Blaster%20Tactic%203D%20Wrath&srnum=2)  

##  Стереоколонки  
![Microlab Pro](http://mdata.yandex.net/i?path=b0522204337__Image_1.jpg)
Например, «Microlab Pro 1» или «Microlab Pro 2»  
[Купить тут](http://market.yandex.ru/search.xml?text=microlab+pro&cvredirect=2)  

### Huawei E5756
![](https://mdata.yandex.net/i?path=b0301141934_img_id3770409832386214367.jpg)
[Посмотреть на Yandex.Market](https://market.yandex.ru/product/9263633?hid=723087&track=tabs)

## MiniDock
![MiniDock by RadTech](http://www.radtech.us/images/product/15212/736211527677-3.jpg)
Ну и аналогичный [мини-док для путешествий](http://www.radtech.us/products/bluelounge-minidock-usb-30pin-dock) (для розеток стандарта US).

## Bluetooth selfie button 
![](http://product-images.imshopping.com/nimblebuy/bluetooth-selfie-button-1009082-2298682-regular.jpg)
Для моих традиционных операторских развлечений.  
Купить можно, [например здесь](http://www.bymobile.ru/bluetoothknopka_dlja_ipad_iphone_samsung_i_htc_dlja_sozdanija_selfie_black/goods/10665/).

## Ну или, сразу селфи-палку с такой кнопкой (что экономичней)
![](http://pup.ru/images/catalog/preview/product_preview_8879.jpg)
[Например, такую.](http://pup.ru/catalog/usb_gadgets/prod-2965/?_openstat=bWFya2V0LnlhbmRleC5ydTvQnNC-0L3QvtC_0L7QtCAo0YjRgtCw0YLQuNCyLCDQv9Cw0LvQutCwKSDQtNC70Y8g0YHQtdC70YTQuCDRgSBCbHVldG9vdGgg0LrQvdC-0L_QutC-0LkgWjA3LTEvQjtJeE5Rc1FYcTJQaXZvNk1jZnlUb2JBOw)

## Стилус Adonit Jot Mini

![](http://static.svyaznoy.ru/upload/iblock/ddd/4111293_2.jpg/resize/400x400/)
Чёрный или серебристый.  
[Например, отсюда.](http://market.yandex.ru/search.xml?hid=91461&text=%D1%81%D1%82%D0%B8%D0%BB%D1%83%D1%81%D1%8B%20jot%20mini&srnum=25&ortext=c%D1%82%D0%B8%D0%BB%D1%83%D1%8B%20jot%20mini)

## Wacon Intuos Personalisation Kit
![](http://cdn.cvp.com/images/products/altimage/12%2012%2020141418385631Wacom%20Intuos%20Personalisation%20Kit.jpg)
[Купить тут](http://cvp.com/index.php?t=product/wacom_wacack40801)

## Переходник Mini Displayport > VGA
![](https://s1.mmr.ms/i/1284224/0/1000x1000.png)
[Вот этот,например](https://www.mediamarkt.ru/item/1284224/isy-imd-1000-adapter-mini-displayport-vga?utm_medium=cpc&utm_content=1284224&location=shop_R001&utm_campaign=yandex_market&utm_source=yandexmarket.ekb&utm_term=comp_nopc_Isy_IMD1000&_openstat=bWFya2V0LnlhbmRleC5ydTvQkNC00LDQv9GC0LXRgCBNaW5pIERpc3BsYXlQb3J0IC0gVkdBIElzeSBJTUQgMTAwMDtmMWhNN1N3NUh6Nk5ycU5TT3hwQkVnOw)

## Триммер для усов и бороды
![](http://s3.thcdn.com/productimg/0/600/600/04/10885504-1387302272-848057.jpg)
Какой-нибудь небольшой и аккуратный. 
Например – [Braun CruZer 6 Precision](http://ekb.onlinetrade.ru/catalogue/trimmer_dlya_litsa_i_tela-c1326/braun/trimmer_dlya_borodi_i_usov_braun_cruzer_6_precision-133061.html?from=yamekb&utm_source=yamarket&utm_medium=cpc&_openstat=bWFya2V0LnlhbmRleC5ydTvQotGA0LjQvNC80LXRgCDQtNC70Y8g0LHQvtGA0L7QtNGLINC4INGD0YHQvtCyIEJSQVVOIENydVplciA2IFByZWNpc2lvbjszbW96YzBqRkNSTXYyTDhmRTZWU1BnOw)

## Не гаджет, но тем не менее
![iPay](http://www.iphones.ru/wp-content/uploads/2012/12/card.jpg)
Подарочные карты App Store. Всегда актуально (я ведь покупаю софт и всё такое).

# Карторисовательное

## Портативный световой стол A3-формата
![](http://ecx.images-amazon.com/images/I/61giKcNUt3L._SL1000_.jpg)
Для рисования руками по бумаге всяких многослойных конструкций.  
[Например, вот этот](http://www.amazon.co.uk/MiniSun-Modern-Ultra-Slim-Design-LightPad/dp/B00AYXR7CW/ref=sr_1_1?s=lighting&ie=UTF8&qid=1433242974&sr=1-1&keywords=MiniSun+A3+LED)

# Разные подкастерские штуки:

## Tascam US-122MKII
![Tascam US-122MKII](http://mdata.yandex.net/i?path=b0325143226_img_id7096962840364666549.jpg)
[Купить тут](http://market.yandex.ru/search.xml?text=Tascam+US-122MKII)  

## Или AKG Perception 120
![AKG Perception 120](http://recordinghacks.com/microphone-photo/00899/AKG-Acoustics/Perception-120-USB.jpg)
[Купить тут](http://market.yandex.ru/search.xml?text=AKG+Perception+120)  

## Поп-фильтр, например
![Simple Pop Filter](http://www.xcom-hobby.ru/var/files/b3/44/51b0aa7b344a8643797695_310.jpg)
[Купить тут](http://www.xcom-hobby.ru/proaudio_hfm050_318905.html)

## И настольную микрофонную стойку 
![Просто, изящно и усточиво](http://konsonans-ekb.ru/upload/iblock/901/90142d9c35a2d47646923837ea6dc037.jpeg)
[Например – такую](http://konsonans-ekb.ru/shop/stoyki_mikrofonnye/stoyka_mikrofonnaya_force_msc_10/)  

# Одежда и аксессуары  

## Пафосную рабочую робу «хозяина подземелья»
![](https://pp.vk.me/c622828/v622828808/342bd/96vhM1CYyeY.jpg)
Из естественных материалов, тёмно-серую или тёмно-красную (бордо), отороченную золотой тесьмой, с глубоким капюшоном, запахивающуюся,  на мальчика с размером торса 44 и ростом 170. Наличие подкладки опционально, но желательно.

Зачем? <s>Во славу...!</s> Например, [чтобы делать так](http://www.kingofrpgs.com/the-street-master-2/).

# Футболки
<small>**Примечание:** Все нижеперечисленные футболки также вполне актуальны в виде «longsleeve» (с длинным рукавом), если такая опция вообще доступна на сайте магазина.</small>

## Футболку «Astral Fear», мужскую, размер «S» от LoTFP
![](http://1.bp.blogspot.com/-kH68H-lNw8w/VjcsYsW1u6I/AAAAAAAAKwU/fyMbkL4lhCs/s400/APPromo2.jpg)
[Купить тут](http://www.lotfp.com/store/index.php?route=product/product&path=41&product_id=201)

## Футболку «Hallucinator», мужскую, размер «S» от LoTFP
![](http://cdn.shopify.com/s/files/1/0180/3191/products/DSC_0549.png?v=1432266107)
[Купить тут](http://shopcriticalhit.com/collections/apparel/products/hallucinator-shirt)

## Футболку «Cthulhoids», мужскую, размер «S» от LoTFP
![](http://cdn.shopify.com/s/files/1/0180/3191/products/FullSizeRender_7.jpg?v=1444937861)
[Купить тут](http://shopcriticalhit.com/collections/apparel/products/cthuloids-shirt)

## Футболку «Dungeon crawlers», мужскую, размер «S»
![](https://cdn-images.threadless.com/threadless-shop/products/3300/1272x920shirt_guys_03.jpg?w=1272&h=920)
[Купить тут](https://www.threadless.com/product/3300) 

## Футболку «Platonic love», мужскую, размер «S»
![](http://shirtoid.com/wp-content/uploads/2013/04/Platonic-Love.jpg)
[Купить тут](http://shirt.woot.com/offers/platonic-love?utm_campaign=Commission+Junction+-+10836837&utm_source=Commission+Junction+Publisher+-+3497954&utm_medium=affiliate+-+shirt.woot) 

## Футболку «Fellowship of the dungeon», мужскую, размер «S»
![](http://shirtoid.com/wp-content/uploads/2015/11/Fellowship-of-the-Dungeon.jpg)
[Купить тут](http://shirt.woot.com/offers/fellowship-of-the-dungeon?utm_campaign=Commission+Junction+-+10836837&utm_source=Commission+Junction+Publisher+-+3497954&utm_medium=affiliate+-+shirt.woot) 

## Футболку «Dungeon mapmaker», мужскую, тёмно-серую, размер «S»
![](http://rlv.zcache.com/svc/view?rlvnet=1&realview=113383014369852142&design=7e0a4ae6-38a8-4984-b356-7a1e0d35dee1&style=basic_dark_tshirt&size=a_xl&color=charcoalheather&max_dim=512&hide=bleed%2Csafe%2CvisibleMask&r=1447316767750)
[Купить тут](http://www.zazzle.com/dark_dungeon_map_marker_image_only_t_shirts-235623131062512378) 

## Футболку «Beholder Metal», мужскую, серого цвета, размер «S»
![](http://image.welovefine.com/image/cache/data/p/02/9102-22910-1000x1000.jpg)
[Купить тут](http://www.welovefine.com/t-shirts-425-434/beholdermetal-9102/sort/p.sort_order.html?order=DESC) 

## Футболку «Me and Ms.Tiamat», мужскую, бежевого цвета, размер «S»
![](http://image.welovefine.com/image/cache/data/p/08/8408-20553-1000x1000.jpg)
[Купить тут](http://www.welovefine.com/t-shirts-425-434/me-ms-tiamat-8408/sort/p.sort_order.html?order=DESC) 

## Футболку «Dragon of few words», мужскую, серого цвета, размер «S»
![](http://image.welovefine.com/image/cache/data/productimages/DnD/Mens_Tees/MDDG133GL1_Dragon%20of%20Few%20Words_CHA-1000x1000.jpg)
[Купить тут](http://www.welovefine.com/t-shirts-425-434/dragon-of-few-words-10325/sort/p.sort_order.html?order=DESC) 

## Футболку «Roll your dice», мужскую, серого цвета, размер «S»
![](https://d3gqasl9vmjfd8.cloudfront.net/dd9a65a4-e826-4442-a165-1ad492376286.png)
[Купить тут](http://shirt.woot.com/offers/roll-your-dice?ref=cnt_ctlg_dgn_8) 

## Футболку «Cubicles & Coworkers», мужскую, серого цвета, размер «S»
![](https://d3gqasl9vmjfd8.cloudfront.net/22d79b69-f9be-4be7-8cff-ad686791da73.png)
[Купить тут](http://shirt.woot.com/offers/cubicles-and-coworkers?ref=cnt_ctlg_dgn_2) 

## Футболку «Hide & Seeker», мужскую, размер «S»
![](https://d3gqasl9vmjfd8.cloudfront.net/237907ef-c4dc-4c5d-9f04-02bc0bd29736.png)
[Купить тут](http://shirt.woot.com/offers/hide-and-seeker?ref=cnt_ctlg_dgn_6) 

## Любую футболку от Shiroi Neko, мужскую, размера «S»
![Nya, desu!](http://t09.co.kr/product_img/20090818/t06/01.jpg)
Точнее, любую [из этого  списка](https://www.evernote.com/shard/s278/sh/7017da53-f9cd-4948-be86-2627fc7a4002/3af1e1f76b23666019606b7dce29068b)  

## Футболку «Talk Nerdy»
![Talk nerdy to me](http://dnd.ryancarlton.com/wp-content/uploads/2010/07/nerdy.jpg)
Мужскую, скромного цвета (некрашеный хлопок, navy blue или средне-серую), размера «S» с вот такой надписью.

## Футболку про D&D Party, мужскую, размера «S»
![](https://pbs.twimg.com/media/BxTkv3pCMAAe-Wp.jpg)

# Кухня  

## Термокружка Starbucks Stainless Steel Tumbler 
![](http://ecx.images-amazon.com/images/I/81AAzD%2BUCML._SL1500_.jpg)
Матовый, чёрный, модели «Stealth» (на фото)  
[Купить тут](http://goo.gl/V0uUMi)

## Казан 
![Аккуратно и эстетично](http://www.ikea.com/ru/ru/images/products/senior-kazan-s-kryskoj__0244446_PE383724_S4.JPG)
[Например Сениор от IKEA](http://www.ikea.com/ru/ru/catalog/products/90232840/) или аналогичный (можно - со стеклянной крышкой)  

## Кастрюля с усиленным днищем 
![Эстетичная оригинальность](http://magnitogorsk.real-com.net/goodspic/9/37/538379/thumbs_700/g_image_502218ebc6e8b.jpg)
[Например "Медаль" от IKEA](http://www.ikea.com/ru/ru/catalog/products/30100442/)  

## Овощерезка 
![Nice sliced!](http://www.znaxidka.com.ua/Templates/storage/Tovars/dlja-kuhni/nicer-dicer-plus.jpg)
[Типа Nicer Dicer Plus](http://market.yandex.ru/search.xml?text=nicer+dicer&cvredirect=2)  

*Качественная и с гарантией (одну дешёвую я уже покалечил)*

## Фартук 
![IKEA Gunsig](http://www.ikea.com/ca/en/images/products/ikea-gunstig-apron-gray__0244820_PE384555_S4.JPG)
[IKEA Гунстиг из серии "365+"](http://www.ikea.com/ru/ru/catalog/products/60182115/)  

## Поварской нож
![Classic Chief nade](http://ikea-club.com.ua/content/images/thumbs/0039367_-.jpeg)
Конкретно – [IKEA Гнистра из серии "365+"](http://www.ikea.com/ru/ru/catalog/products/20149321/)  

## Поварской нож-топорик 
![Chop-chop-chop](http://www.ikea.com/PIAimages/0245184_PE384479_S5.JPG)
[Тоже от IKEA](http://www.ikea.com/ru/ru/catalog/products/30233442/)  

# DIY  

## Коврик для резки самовосстанавливающийся
![Нужная, между прочим вещь](http://www.shop-tilda.ru/images/201305/goods_img/18995_G_1368840681735.jpg)
Скажем, А3 формата (или около того).
Купить можно, например в [komus.ru](http://komus.ru)

## Пила ручная, складная-садовая
![](http://www.bohero.eu/Repository/Cached/ProductPictures_Fiskars_Garden_V1/FISKARS_123860_123870_123880-canvas-640.jpg)
[Fiskars Xtract™ SW75](http://market.yandex.ru/search.xml?hid=90666&text=fiskars%20sw75&srnum=3&ortext=fiskars%20sw75)  
Для походов в леса – самое оно.

## Набор для заточки инструмента от Lansky
![Весчь!](http://www.sld.ru/imgtmk7.php?pic=files/good_big/277_00000000.jpg&v=0) 
[Суперская штука](http://www.sld.ru/catalog/nozhi/prisposobleniya-dlya-zatochki/nabor-dlya-zatochki-lansky-lkc03-standard.html)

# Кулинария

## Мате/кудин и ройбуш
![](http://povarusha.ru/foto/mate-4aj.jpg)
Т.к. постоянно забываю его купить.

# Прочее

## Подарочные сертификаты в какой-нибудь екатеринбуржский Tattoo-салон
![sample blackwork](https://c2.staticflickr.com/4/3535/5795678761_36bc8c2a91_z.jpg)
Желательно тот, в котором хорошо умеют работать с [blackwork](http://www.furfur.me/furfur/all/culture/164137-blackwork) по эскизам заказчика.

## Сертификаты-билеты на IT-курсы
![](http://cdn2.itpro.co.uk/sites/itpro/files/styles/article_main_wide_image/public/images/dir_213/it_photo_106813.jpg?itok=l4cxgUxU)
Особенно, если они связаны с Unix/Linux и сетями.

## Сертификаты на языковые курсы (английского языка)
![](http://www.dayjob.com/images/pic_evening_language_courses_london.jpg)
Онлайн или локально в Екатеринбурге.

## Сертификаты на курсы по изучению традиционных ремёсел
![](http://ceramic-studio.net/images/slike/44/home-ceramic-02__pocetna.jpg)
Лепка, керамика, плетение из лозы/бересты, плотницкие работы, вот это всё.

## Банджо (и колечки к нему)
![sample banjo](http://upload.wikimedia.org/wikipedia/commons/3/30/Don_Wayne_Reno_playing_the_banjo_with_fingerpicks.jpg)
Cерьёзно, мэн. Банджо это супер.
