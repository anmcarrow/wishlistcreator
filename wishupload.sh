#!/bin/bash

# 2015, BSDL, am_ (mailbox@anmcarrow.me)
# Requirements: `markdown` app (just use `apt-get install markdown` or same stuff)

REMOTE="user@host/path/to/sitedir"

cat header.txt > wishlist.html
markdown wishlist.md >> wishlist.html
cat footer.txt >> wishlist.html

rsync -av --delete -e ssh  ./style.css ./*.html $REMOTE/

exit 0
