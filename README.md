This is a simple set of examle scripts for generating clear and
lite HTML static webpage for self-hosted wishlist (list of the wishes and wanted presents)
from Markdown file, and Rsync+SSH upload this wishlist to the remote
webhosting.

Shellscript use `markdown`, `rsync` and `openssh-client` utilites.

#### List of content:
- footer.txt — HTML footer template;
- header.txt — HTML header template;
- README.md — this file;
- screenshot.png — sample of wishlist design;
- style.css — CSS design stylesheets;
- wishlist.md — sample of the Markdown wishlist document (in Russian);
- wishupload.sh — shellscript for convertation and uploading your wishlist.
